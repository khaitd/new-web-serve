#include "pch.h"
#define _CRT_SECURE_NO_WARNINGS
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include "stdio.h"
#include "winsock2.h"
#include"time.h"


DWORD WINAPI ClientThread(LPVOID);
char* substr(char* s, int start, int end);
int findsubstr(const char* str, char* sub);

SOCKET clients[64];
char* ids[64];
int numClients;
time_t tick;
char *client_ip;
int client_port;
char times[100];
char userCurrent[100] = "";
char userCurrentPermission[100] = "";
int main()
{
	WSADATA wsa;
	WSAStartup(MAKEWORD(2, 2), &wsa);

	SOCKET listener = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	SOCKADDR_IN ClientAddr;
	SOCKADDR_IN addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_port = htons(9000);

	bind(listener, (SOCKADDR*)&addr, sizeof(addr));
	listen(listener, 5);
	
	numClients = 0;
	
	while (1)
	{
		int len = sizeof(ClientAddr);
		SOCKET client = accept(listener, (SOCKADDR*)&ClientAddr, &len);
		client_ip = inet_ntoa(ClientAddr.sin_addr);
		client_port = ntohs(ClientAddr.sin_port);
		printf("New client accepted: %d:\n", client);
		printf("Peer IP address: %s:%d\n",client_ip,client_port);
		tick = time(NULL);
		snprintf(times, sizeof(times)-1, "%s", ctime(&tick));
		printf("%s", times);
		CreateThread(0, 0, ClientThread, &client, 0, 0);
	}
}

DWORD WINAPI ClientThread(LPVOID lpParam)
{
	SOCKET client = *(SOCKET*)lpParam;
	char buf[5000];
	int ret;
	char get[500], path[500], http[500], target_id[64];

	while (1)
	{
		ret = recv(client, buf, sizeof(buf), 0);
		if (ret <= 0)
			return 1;

		buf[ret] = 0;
		printf("Received: %s", buf);

		ret = sscanf(buf, "%s %s %s", get, path, http);

		char pathGetRegister[100];
		char pathPostRegister[100];
		char pathGetLogin[100];
		char pathPostLogin[100];
		char pathGetList[100];
		char pathGetEdit[100];
		char userEdit[50];
		char pathPostEdit[100];
		char pathPostDelete[100];
		char userDelete[50];
		char pathGetLog[100];
		char js[1000] = "";

		if (strstr(path, "/register") != NULL) {
			char* pathRegister = strstr(path, "/register");
			int sizePathRegister = strlen(pathRegister);

			if (sizePathRegister == 9) {
				strcpy(pathGetRegister, pathRegister);
			}
			else if (sizePathRegister > 9)
			{
				strcpy(pathPostRegister, pathRegister);
			}
		}
		else if (strstr(path, "/login") != NULL) {
			char* pathLogin = strstr(path, "/login");
			int sizePathLogin = strlen(pathLogin);

			if (sizePathLogin == 6) {
				strcpy(pathGetLogin, pathLogin);
			}
			else if (sizePathLogin > 6)
			{
				strcpy(pathPostLogin, pathLogin);
			}
		}
		else if (strstr(path, "/list") != NULL) {
			char* pathList = strstr(path, "/list");
			int sizePathList = strlen(pathList);

			if (sizePathList == 5) {
				strcpy(pathGetList, pathList);
			}
		}
		else if (strstr(path, "/edit?username=") != NULL && strstr(path, "&password=") == NULL) {
			strcpy(pathGetEdit, path);
			strcpy(userEdit, substr(pathGetEdit, 15, strlen(pathGetEdit)));
		}
		else if (strstr(path, "/edit?username=") != NULL && strstr(path, "&password=") != NULL) {
			strcpy(pathPostEdit, path);
		}
		else if (strstr(path, "/delete?username=") != NULL) {
			strcpy(pathPostDelete, path);
			strcpy(userDelete, substr(pathPostDelete, 17, strlen(pathPostDelete)));
		}
		else if (strstr(path, "/log") != NULL) {
			char* pathLog = strstr(path, "/log");
			int sizePathLog = strlen(pathLog);

			if (sizePathLog == 4) {
				strcpy(pathGetLog, pathLog);
			}
		}
		else {

		}

		if ((strcmp(get, "GET") == 0) & (strcmp(path, pathGetRegister) == 0))
		{
			char html[100000];
			char header[1000] = "HTTP/1.1 200 OK\nContent-Type: text/html;charset = utf-8\n\n";
			strcpy(html, header);
			char layout[5000];
			FILE* f = fopen("page//register.html", "r");
			int h1 = fread(layout, 1, sizeof(layout), f);
			//layout[h1] = 0;
			strcat(html, layout);
			fclose(f);
			send(client, html, strlen(html), 0);
		}
		else if ((strcmp(get, "GET") == 0) & (strcmp(path, pathPostRegister) == 0))
		{
			char mess[150];
			char username[150];
			char pasword[150];
			char* pass = strstr(path, "password=");
			char* regis = strstr(path, "register=");
			int locationPass = findsubstr(path, pass);
			int locationRegis = findsubstr(path, regis);
			char* getUser = substr(path, 19, locationPass - 2);
			sprintf(username, "%s", getUser);
			char* getPass = substr(path, locationPass + 9, locationRegis - 2);
			sprintf(pasword, "%s", getPass);

			char txtAccount[5000];
			char checkUser[200] = "/";
			strcat(checkUser, username);
			strcat(checkUser, " ");

			FILE* f = fopen("account.txt", "a+");
			int h1 = fread(txtAccount, 1, sizeof(txtAccount), f);
			char* check = strstr(txtAccount, checkUser);

			if (check == NULL) {
				fprintf(f, "%s", username);
				fprintf(f, "%s", " - ");
				fprintf(f, "%s", pasword);
				fprintf(f, "%s", " - ");
				fprintf(f, "%s", "user");
				fprintf(f, "%s", " /");
				fclose(f);
				strcpy(mess, "<h2 style=\"color:#00CCFF;text-align: center\">Đăng ký thành công");
				char log[10240];
				FILE* f1 = fopen("log.txt", "a+");
				int h2 = fread(log, 1, sizeof(log), f1);
				fprintf(f1, "%s", username);
				fprintf(f1, "%s", " - ");
				fprintf(f1, "%s", client_ip);
				fprintf(f1, "%s", ":");
				fprintf(f1, "%d", client_port);
				fprintf(f1, "%s", " - ");
				char *tmp = substr(times, 0, 23);
				fprintf(f1, "%s", tmp);
				fprintf(f1, "%s", " - register");
				fprintf(f1, "%s", " /");
				fclose(f1);
			}
			else {
				strcpy(mess, "<h2 style=\"color:red;text-align: center\">Tài khoản đã tồn tại");
			}

			char html[50000];
			char header[1000] = "HTTP/1.1 200 OK\nContent-Type: text/html;charset = utf-8\n\n";
			char body[5000] = "<html><head><meta charset=\"UTF-8\"><meta name=\"viewport\" content =\"width=device-width, initial-scale=1.0\"><meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\"><title>Register</title></head><body>";
			strcpy(html, header);
			strcat(html, body);
			strcat(html, mess);
			strcat(html, "</h2>");
			strcat(html, "</body></html>");

			char layout[5000];
			FILE* f2 = fopen("page//register.html", "r");
			int h2 = fread(layout, 1, sizeof(layout), f2);
			layout[h2] = 0;
			strcat(html, layout);
			fclose(f2);
			send(client, html, strlen(html), 0);
		}
		else if ((strcmp(get, "GET") == 0) & (strcmp(path, pathGetLogin) == 0))
		{
			
			char html[100000];
			char header[1000] = "HTTP/1.1 200 OK\nContent-Type: text/html;charset = utf8\n\n";
			strcpy(html, header);
			
			char layout[5000];
			FILE* f = fopen("page//login.html", "r");
			
			int h1 = fread(layout, 1, sizeof(layout), f);
			//layout[h1] = 0;
			strcat(html, layout);
			fclose(f);

			send(client, html, strlen(html), 0);
		}
		else if ((strcmp(get, "GET") == 0) & (strcmp(path, pathPostLogin) == 0))
		{
			char mess[150];
			char username[150];
			char password[150];
			char* pass = strstr(path, "password=");
			char* log = strstr(path, "login=");
			int locationPass = findsubstr(path, pass);
			int locationLog = findsubstr(path, log);
			char* getUser = substr(path, 16, locationPass - 2);
			sprintf(username, "%s", getUser);
			char* getPass = substr(path, locationPass + 9, locationLog - 2);
			sprintf(password, "%s", getPass);

			char txtAccount[5000];
			char checkAccount[150] = "/";
			strcat(checkAccount, username);
			strcat(checkAccount, " - ");
			strcat(checkAccount, password);
			strcat(checkAccount, " - ");

			FILE* f = fopen("account.txt", "r");
			int h1 = fread(txtAccount, 1, sizeof(txtAccount), f);
			txtAccount[h1] = 0;
			fclose(f);
			char tmpUser[150];
			char tmpPass[150];
			char tmpPermission[50];
			char find[3] = " /";
			char find1[] = " - ";
			char* checkUsername1 = strstr(txtAccount, checkAccount);

			if (checkUsername1 != NULL) {
				strcpy(checkUsername1, substr(checkUsername1, 0, findsubstr(checkUsername1, find)));
				char* checkUsername2 = substr(checkUsername1, 1, strlen(username));
				sprintf(tmpUser, "%s", checkUsername2);

				char* checkPassword = substr(checkUsername1, strlen(tmpUser) + 4, strlen(checkUsername1));
				sprintf(tmpPass, "%s", substr(checkPassword, 0, findsubstr(checkPassword, find1) - 1));

				char* checkPermission = substr(checkUsername1, strlen(tmpUser) + strlen(tmpPass) + 7, strlen(checkUsername1) - 2);
				sprintf(tmpPermission, "%s", checkPermission);
			}
			else {
				strcpy(tmpUser, "");
				strcpy(tmpPass, "");
				strcpy(tmpPermission, "");
			}

			if (strcmp(tmpUser, "") == 0) {
				strcpy(userCurrent, "");
				strcpy(userCurrentPermission, "");
				strcpy(mess, "<h2 style=\"color: red;text-align:center\">Tài khoản hoặc mật khẩu không đúng");
			}
			else if ((strcmp(tmpUser, username) == 0) && (strcmp(tmpPass, password) != 0)) {
				strcpy(userCurrent, "");
				strcpy(userCurrentPermission, "");
				strcpy(mess, "<h2 style=\"color: red;text-align:center\">Tài khoản hoặc mật khẩu không đúng");
			}
			else if ((strcmp(tmpUser, username) == 0) && (strcmp(tmpPass, password) == 0)) {
				strcpy(userCurrent, tmpUser);
				strcpy(userCurrentPermission, tmpPermission);
				char log[10240];
				FILE* f1 = fopen("log.txt", "a+");
				int h2 = fread(log, 1, sizeof(log), f1);
				fprintf(f1, "%s", tmpUser);
				fprintf(f1, "%s", " - ");
				fprintf(f1, "%s", client_ip);
				fprintf(f1, "%s", ":");
				fprintf(f1, "%d", client_port);
				fprintf(f1, "%s", " - ");
				char *tmp = substr(times, 0, 23);
				fprintf(f1, "%s", tmp);
				fprintf(f1, "%s", " - login");
				fprintf(f1, "%s", " /");
				mess[0] = 0;
				strcpy(js, "<script>window.location=\"/list\";</script>");
				fclose(f1);
			}

			char html[50000];
			char header[1000] = "HTTP/1.1 200 OK\nContent-Type: text/html;charset = utf-8\n\n";
			char body[500] = "<html><head><meta charset=\"UTF-8\"><meta name=\"viewport\" content =\"width=device-width, initial-scale=1.0\"><meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\"><title>Register</title></head><body>";
			strcpy(html, header);
			strcat(html, body);
			strcat(html, js);
			strcat(html, "</head><body>");
			strcat(html, mess);
			strcat(html, "</h2>");
			strcat(html, "</body></html>");

			char layout[5000];
			FILE* f2 = fopen("page//login.html", "r");
			int h2 = fread(layout, 1, sizeof(layout), f2);
			//layout[h2] = 0;
			strcat(html, layout);
			fclose(f2);

			send(client, html, strlen(html), 0);
		}
		else if ((strcmp(get, "GET") == 0) & (strcmp(path, pathGetList) == 0))
		{
			char txtAccount[5000];
			char html[50000];
			char header[1000] = "HTTP/1.1 200 OK\nContent-Type: text/html;charset=utf-8\n\n";
			strcpy(html, header);
			char layout[5000];
			FILE* f2 = fopen("page//home.html", "r");
			int h2 = fread(layout, 1, sizeof(layout), f2);
			layout[h2] = 0;
			strcat(html, layout);
			fclose(f2);
			strcat(html, userCurrent);
			strcat(html, " (");
			strcat(html, userCurrentPermission);
			strcat(html, ")");
			strcat(html, "</label>");
			strcat(html, "</label><a href=\"/login\"><button>Đăng xuất</button></a></div></div>");
			strcat(html, "<div class=\"container\"><h2>Danh sách tài khoản</h2><table><tr><th>Tài khoản</th><th>Mật khẩu</th><th>Tùy chỉnh</th></tr>");

			FILE* f = fopen("account.txt", "r");
			fgets(txtAccount, sizeof(txtAccount), f);
			fclose(f);
			char find1[3] = " /";
			char find2[4] = " - ";

			while (1) {
				int h = findsubstr(txtAccount, find1);
				if (h == -1) {
					break;
				}

				char a1[5000];
				char a2[1000];
				char* str1 = substr(txtAccount, 1, h);
				strcpy(a1, str1);
				strcat(html, "<tr><td>");
				strcat(html, substr(a1, 0, findsubstr(a1, find2) - 1));
				strcat(html, "</td><td>");

				if (strcmp(userCurrentPermission, "admin") == 0) {
					strcpy(a2, substr(a1, findsubstr(a1, find2) + 3, strlen(a1)));
					strcpy(a2, substr(a2, 0, findsubstr(a2, find2) - 1));
					strcat(html, a2);
				}
				else {
					strcat(html, "*****");
				}
				strcat(html, "</td>");

				if ((strcmp(userCurrentPermission, "admin") == 0) || (strcmp(userCurrentPermission, "user") == 0 && strcmp(userCurrent, substr(a1, 0, findsubstr(a1, find2) - 1)) == 0)) {
					strcat(html, "<td><a href=\"/edit?username=");
					strcat(html, substr(a1, 0, findsubstr(a1, find2) - 1));
					strcat(html, "\">Sửa</a>");
					if (strcmp(userCurrentPermission, "admin") == 0 && strcmp(userCurrent, substr(a1, 0, findsubstr(a1, find2) - 1)) != 0) {
						strcat(html, " - <a href=\"/delete?username=");
						strcat(html, substr(a1, 0, findsubstr(a1, find2) - 1));
						strcat(html, "\">Xóa</a></td></tr>");
					}
				}
				else {
					strcat(html, "<td></td></tr>");
				}
				char tmpTxt[1000];
				char* str2 = substr(txtAccount, h + 1, strlen(txtAccount));
				strcpy(tmpTxt, str2);
				strcpy(txtAccount, tmpTxt);
			}
			strcat(html, "</table><div class=\"log\"><a href=\"/log\"><button>Nhật ký</button></a></div></div></body></html>");

			send(client, html, strlen(html), 0);
		}
		else if ((strcmp(get, "GET") == 0) & (strcmp(path, pathGetEdit) == 0))
		{
			char html[50000];
			char header[1000] = "HTTP/1.1 200 OK\nContent-Type: text/html;charset = utf8\n\n";
			strcpy(html, header);
			char layout[5000];

			FILE* f2 = fopen("page//edit.html", "r");
			int h2 = fread(layout, 1, sizeof(layout), f2);
			layout[h2] = 0;
			strcat(html, layout);
			fclose(f2);
			strcat(html, userEdit);
			strcat(html, " \" name=\"username\" required readonly ><label for=\"psw\"><b>Mật khẩu</b></label><input type=\"password\" placeholder=\"Nhập mật khẩu\" name =\"password\" required><br><br><br><div class = \"clearfix\"><button type =\"submit\" class =\"editbtn\" name =\"edit\">Submit</button><a href =\"/list\"><div class=\"cancelbtn\">cancel</div></a></div></div></form></body></html>");

			send(client, html, strlen(html), 0);
		}
		else if ((strcmp(get, "GET") == 0) & (strcmp(path, pathPostEdit) == 0))
		{
			char mess[150];
			char username[150];
			char pasword[150];
			char* pass = strstr(path, "password=");
			char* edit = strstr(path, "edit=");
			int locationPass = findsubstr(path, pass);
			int locationEdit = findsubstr(path, edit);
			char* getUser = substr(path, 15, locationPass - 3);
			sprintf(username, "%s", getUser);
			char* getPass = substr(path, locationPass + 9, locationEdit - 2);
			sprintf(pasword, "%s", getPass);

			char txtAccount[5000];
			char txt1[5000];
			char txtChange[5000];
			char txt2[5000];
			char find[3] = " /";

			char checkUser[200] = "/";
			strcat(checkUser, username);
			strcat(checkUser, " ");

			FILE* f = fopen("account.txt", "a+");
			int h1 = fread(txtAccount, 1, sizeof(txtAccount), f);
			fclose(f);
			char* check = strstr(txtAccount, checkUser);

			if (check != NULL) {
				int locationAcc = findsubstr(txtAccount, checkUser);
				strcpy(txt1, substr(txtAccount, 0, locationAcc));

				int d1 = strlen(txt1);

				strcpy(txt2, substr(txtAccount, d1, h1 - 1));
				strcpy(txt2, substr(txt2, findsubstr(txt2, find) + 2, strlen(txt2)));

				strcpy(txtChange, username);
				strcat(txtChange, " - ");
				strcat(txtChange, pasword);
				strcat(txtChange, " - ");
				strcat(txtChange, "user");
				strcat(txtChange, " /");

				strcpy(txtAccount, txt1);
				strcat(txtAccount, txtChange);
				strcat(txtAccount, txt2);

				FILE* f = fopen("account.txt", "w");
				fprintf(f, "%s", txtAccount);
				fclose(f);
				strcpy(mess, "<h2 style=\"color:#00CCFF;text-align: center\">Sửa thành công");

				char log[10240];
				FILE* f1 = fopen("log.txt", "a+");
				int h2 = fread(log, 1, sizeof(log), f1);
				fprintf(f1, "%s", username);
				fprintf(f1, "%s", " - ");
				fprintf(f1, "%s", client_ip);
				fprintf(f1, "%s", ":");
				fprintf(f1, "%d", client_port);
				fprintf(f1, "%s", " - ");
				char *tmp = substr(times, 0, 23);
				fprintf(f1, "%s", tmp);
				fprintf(f1, "%s", " - edit");
				fprintf(f1, "%s", " /");
				fclose(f1);
				
			}
			else {
				strcpy(mess, "<h2 style =\"color:#00CCFF;text-align: center\">Sửa không thành công");
			}

			char html[50000];
			char header[1000] = "HTTP/1.1 200 OK\nContent-Type: text/html;charset = utf8\n\n";
			char body[5000] = "<html><head><meta charset=\"UTF-8\"></head><body>";
			strcpy(html, header);
			strcat(html, body);
			strcat(html, mess);
			strcat(html, "</h2>");
			strcat(html, "</body></html>");

			char layout[5000];
			FILE* f2 = fopen("page//edit.html", "r");
			int h2 = fread(layout, 1, sizeof(layout), f2);
			layout[h2] = 0;
			strcat(html, layout);
			fclose(f2);
			strcat(html, " \" name=\"username\" required readonly ><label for=\"psw\"><b>Mật khẩu</b></label><input type=\"password\" placeholder=\"Nhập mật khẩu\" name =\"password\" required><br><br><br><div class = \"clearfix\"><button type =\"submit\" class =\"editbtn\" name =\"edit\">Submit</button><a href =\"/list\"><div class=\"cancelbtn\">cancel</div></a></div></div></form></body></html>");

			send(client, html, strlen(html), 0);
		}
		else if ((strcmp(get, "GET") == 0) & (strcmp(path, pathPostDelete) == 0))
		{
			char mess[150];
			char username[150];
			char txtAccount[5000];
			char txt1[5000];
			char txt2[5000];
			char find[3] = " /";

			char checkUser[200] = "/";
			strcat(checkUser, userDelete);
			strcat(checkUser, " ");

			FILE* f = fopen("account.txt", "a+");
			int h1 = fread(txtAccount, 1, sizeof(txtAccount), f);
			fclose(f);
			char* check = strstr(txtAccount, checkUser);

			if (check != NULL) {
				int locationAcc = findsubstr(txtAccount, checkUser);
				strcpy(txt1, substr(txtAccount, 0, locationAcc));

				int d1 = strlen(txt1);

				strcpy(txt2, substr(txtAccount, d1, h1 - 1));
				strcpy(txt2, substr(txt2, findsubstr(txt2, find) + 2, strlen(txt2)));

				strcpy(txtAccount, txt1);
				strcat(txtAccount, txt2);

				FILE* f = fopen("account.txt", "w");
				fprintf(f, "%s", txtAccount);
				fclose(f);

				char log[10240];
				FILE* f1 = fopen("log.txt", "a+");
				int h2 = fread(log, 1, sizeof(log), f1);
				fprintf(f1, "%s", userDelete);
				fprintf(f1, "%s", " - ");
				fprintf(f1, "%s", client_ip);
				fprintf(f1, "%s", ":");
				fprintf(f1, "%d", client_port);
				fprintf(f1, "%s", " - ");
				char *tmp = substr(times, 0, 23);
				fprintf(f1, "%s", tmp);
				fprintf(f1, "%s", " - delete");
				fprintf(f1, "%s", " /");
				fclose(f1);

				strcpy(js, "<script>window.location=\"/list\";</script>");
				strcpy(mess, "Edit success");
			}
			else {
				strcpy(mess, "Error");
			}

			char html[50000];
			char header[1000] = "HTTP/1.1 200 OK\nContent-Type: text/html;charset = utf8\n\n";
			char body[5000] = "<html><head><meta charset=\"UTF-8\"><meta name=\"viewport\" content =\"width=device-width, initial-scale=1.0\"><meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\"><title>Delete</title> ";
			strcpy(html, header);
			strcat(html, body);
			strcat(html, js);
			strcat(html, "</head><body>");
			strcat(html, "<h5 style=\"color: red;\">");
			strcat(html, mess);
			strcat(html, "</h5>");
			strcat(html, "</body></html>");

			send(client, html, strlen(html), 0);
		}
		else if ((strcmp(get, "GET") == 0) & (strcmp(path, pathGetLog) == 0))
		{
		char txtAccount1[5000];
		char html[100000];
		char header1[1000] = "HTTP/1.1 200 OK\nContent-Type: text/html;charset = utf8\n\n";
		strcpy(html, header1);
		char layout[5000];
		FILE* f2 = fopen("page//log.html", "r");
		int h2 = fread(layout, 1, sizeof(layout), f2);
		layout[h2] = 0;
		strcat(html, layout);
		fclose(f2);
		strcat(html, userCurrent);
		strcat(html, " (");
		strcat(html, userCurrentPermission);
		strcat(html, ")");
		strcat(html, "</label>");
		strcat(html, "</label><a href=\"/login\"><button>Đăng xuất</button></a></div></div>");
		strcat(html, "<div class=\"container\"><h2>Nhật ký</h2><table><tr><th>Tài khoản</th><th>Địa chỉ IP</th><th>Thời gian</th><th>Hành động</th></tr>");
		FILE* f1 = fopen("log.txt", "r");
		fgets(txtAccount1, sizeof(txtAccount1), f1);

		char find1[3] = " /";
		char find2[2] = "-";

		while (1) {
			int h = findsubstr(txtAccount1, find1);
			if (h == -1) {
				break;
			}

			char a1[5120];
			char *tmp;
			char *str1 = substr(txtAccount1, 1, h - 1);
			strcpy(a1, str1);

			strcat(html, "<tr><td>");
			strcat(html, substr(a1, 0, findsubstr(a1, find2) - 2));
			tmp = substr(a1, findsubstr(a1, find2) + 2, strlen(a1));
			strcpy(a1, tmp);

			strcat(html, "</td><td>");
			strcat(html, substr(a1, 0, findsubstr(a1, find2) - 2));
			tmp = substr(a1, findsubstr(a1, find2) + 2, strlen(a1));
			strcpy(a1, tmp);

			strcat(html, "</td><td>");
			strcat(html, substr(a1, 0, findsubstr(a1, find2) - 2));
			tmp = substr(a1, findsubstr(a1, find2) + 2, strlen(a1));
			strcpy(a1, tmp);
			strcat(html, "</td><td>");
			strcat(html, substr(a1, 0, strlen(a1)));
			strcat(html, "</td></tr>");
			char* str3 = substr(txtAccount1, h + 1, strlen(txtAccount1));
			char tmpTxt[5000];
			strcpy(tmpTxt, str3);
			strcpy(txtAccount1, tmpTxt);
		}

		strcat(html, "</table></table><div class=\"log\"><a href=\"/list\"><button>Home</button></a></div></div></body></html>");

		send(client, html, strlen(html), 0);
		fclose(f1);
		}
	}
}

char* substr(char* s, int start, int end)
{
	static char p[5000];
	int indext = 0;

	for (int i = start; i <= end; i++)
	{
		p[indext] = s[i];
		indext++;
	}
	p[indext] = '\0';

	return p;
}

int findsubstr(const char* str, char* sub)
{
	const char* p = str;
	int len = strlen(sub);
	while (*p != NULL)
	{
		if (strlen(p) >= len)
		{
			if (strncmp(p, sub, strlen(sub)) == 0)
			{
				return (p - str);
			}
		}
		else
		{
			break;
		}
		p++;
	}
	return -1;
}